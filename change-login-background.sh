#!/bin/bash

urlConfFile="/etc/gdm3/greeter.gconf-defaults"
urlGdmBackgrounds="/usr/share/images/desktop-base/"

echo " "
echo "-> This Script changes background image at GDM-Login-Screen (tested on Debian) you need to run it as root";
echo "-> Please copy the image that you want to put (jpg Extensión) in the same folder that this script is running";
echo ""
echo "------ .jpg Files in directory `pwd` -------"
counter=0

#ls to jpg files section:
for file in *.jpg; 
	do
			if [ -f "$file" ]
			then
				echo "$counter- $file"
				counter=`expr $counter + 1`
			

			else
				echo "No jpg files in this directory."
				echo "Copy a jpg image file and run it again, Bye."
				exit
			fi

			

	done
#select a number of file from ls
echo "Select one from the list by num:"; 
			read selectedFile

counter=0

#Update the greeter.gconf-defaults section:
for fileName in *.jpg; 
	do
			if [ "$selectedFile" = "$counter" ]
				then 
			#remove the lines from file if exist before
				sed -i '/\/background\/picture_filename/ d' $urlConfFile
				sed -i '/#This line generated from Energy1011/ d' $urlConfFile

			#add the new lines at the end of file with filename
				echo "-space-" >> $urlConfFile
				echo "#This line generated from Energy1011 change-login-background script " >> $urlConfFile
				echo /desktop/gnome/background/picture_filename	"$urlGdmBackgrounds""$fileName" >> $urlConfFile
				sed -i '/-space-/ d' $urlConfFile

			#copy the file
				echo "You need to be root to copy the file:$filename to $urlGdmBackgrounds"
			#this script needs to run as root to cp
				 cp "$fileName" "$urlGdmBackgrounds"

				echo "$fileName Sucessfull installed :D"
				echo "Restart your computer to see the changes, bye."
				exit
			fi

			counter=`expr $counter + 1`
	done

	echo "Invalid number, please use a number from the list, run it again."